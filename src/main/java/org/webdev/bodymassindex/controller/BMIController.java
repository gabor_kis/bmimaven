package org.webdev.bodymassindex.controller;

import org.webdev.bodymassindex.model.BmiCalculator;
import org.webdev.bodymassindex.model.BodyMassIndex;
import org.webdev.bodymassindex.model.exceptions.IllegalBMIArgumentException;

/**
 * @author Gabor Kis
 * This is a controller class. 
 * The API is accessible through this class.
 * There are two implementation, we can get the result 
 * either trough the constructors
 * or calling the getBmiCategory(...) methods.
 */
public class BMIController {

	private BodyMassIndex bmiCalc;
	private String bmi;
	
	public BMIController() {
		
	}
	
	/**
	 * When we get the result by calling the constructor with parameters, 
	 * the constructor sets up the expected string
	 * using metric units
	 * @param height
	 * @param weight
	 */
	public BMIController(double height, double weight) {
		
		try {
			bmiCalc = new BmiCalculator(height, weight);
			bmi = bmiCalc.getCategory();
		} catch (IllegalBMIArgumentException e) {
			bmi = e.toString();			
		}		
	}
	
	/**
	 * Using US units
	 * @param heightFoot
	 * @param heightInch
	 * @param weightPound
	 */
	public BMIController(double heightFoot, double heightInch, double weightPound) {
		
		try {
			bmiCalc = new BmiCalculator(heightFoot, heightInch, weightPound);
			bmi = bmiCalc.getCategory();
		} catch (IllegalBMIArgumentException e) {
			bmi = e.toString();			
		}		
	}

	public String getBmi() {
		return bmi;
	}
	
	/**
	 * The other way to get the expected string is calling these methods
	 * which return with the category
	 * ** I think this is the better solution **
	 * Using metric units
	 * @param height
	 * @param weight
	 * @return category string with bmi value
	 */
	public String getBmiCategory(double height, double weight) {
		String bmiCat = null;
		
		try {
			bmiCalc = new BmiCalculator(height, weight);
			bmiCat = bmiCalc.getCategory();
		} catch (IllegalBMIArgumentException e) {
			bmiCat = e.toString();			
		}	
		
		return bmiCat;
	}
	
	/**
	 * Using US units
	 * @param heightFoot
	 * @param heightInch
	 * @param weightPound
	 * @return category string with bmi value
	 */
	public String getBmiCategory(double heightFoot, double heightInch, double weightPound) {
		String bmiCat = null;
		
		try {
			bmiCalc = new BmiCalculator(heightFoot, heightInch, weightPound);
			bmiCat = bmiCalc.getCategory();
		} catch (IllegalBMIArgumentException e) {
			bmiCat = e.toString();			
		}	
		
		return bmiCat;
	}
	
}
