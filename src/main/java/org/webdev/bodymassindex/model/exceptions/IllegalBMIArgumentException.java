package org.webdev.bodymassindex.model.exceptions;

import org.webdev.bodymassindex.model.Converter;

/**
 * This class is to demonstrate a user-defined exception class 
 * @author Gabor Kis
 *
 */
public class IllegalBMIArgumentException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8701369700699639861L;

	private double height;
	private double weight;
	
	public IllegalBMIArgumentException(double height, double weight){
		this.height = height;
		this.weight = weight;
	}

	public IllegalBMIArgumentException(double heightFoot, double heightInch, double weightPound){
		Converter c = new Converter();
		this.height = c.convertFootAndInchToMeter(heightFoot, heightInch);
		this.weight = c.convertPoundToKg(weightPound);
	}
	
	
	/* (non-Javadoc)
	 * user can get feedback of the given and wrong data
	 */
	public String toString(){
		return "Not valid height (" + height * 100 + "cm) or weight (" + weight + "kg) value";
	}
	
}
